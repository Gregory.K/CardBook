~5981408


<!-- Fill in all relevant sections below and delete the ones you don't need. -->

## problem to solve


## :bulb: idea for improvement

please describe your idea (in English)


## conceptual sketch/screenshot

A conceptual screenshot or a simple pen-and-paper drawing would help enormously.  
A picture is worth a thousand words. You can use https://sketch.io/sketchpad


## benefits
1. 
2. 
3. 

## purpose, use cases, goals


## links
* 
* 


/label ~5981408