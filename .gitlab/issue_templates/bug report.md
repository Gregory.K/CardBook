Please read our FAQ: https://gitlab.com/CardBook/CardBook/wikis/FAQ
and make sure your bug isn't a duplicate by searching within the open issues: 
https://gitlab.com/CardBook/CardBook/issues

<!-- If your bug is new, please describe it here (in English).
Fill in ALL relevant sections below and delete inapplicable ones.
Screenshots and log files may be helpful (see below). -->


~5981406

## summary


## screenshot


## steps to reproduce the bug
1. 
2. 
3. 

## current behavior
(what actually happens following the steps above)


## expected behavior
(what you should see instead)


## my version numbers
* CardBook: 
* Thunderbird: 
* OS: 

## CardBook log
````````````````````````````````````````````````
CardBook tab > status bar > click on log entry
increase log size: https://gitlab.com/CardBook/CardBook/wikis/FAQ#4
* copy/paste the log here *
````````````````````````````````````````````````


## Thunderbird console log
```````````````````````````
press CTRL+SHIFT+J
* copy/paste the log here *
```````````````````````````




/label ~5981406
