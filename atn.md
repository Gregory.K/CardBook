### 1. Title
cs: CardBook<br>
da: CardBook<br>
de: CardBook<br>
el: Επαφές (CardBook)<br>
en-US: **CardBook**<br>
es: CardBook<br>
fr: CardBook<br>
hr: <br>
hu: Névjegyzék (CardBook)<br>
id: Kontak (CardBook)<br>
it: CardBook<br>
ja: 連絡先「CardBook」<br>
ko: 연락처 (CardBook)<br>
lt: <br>
nl: CardBook<br>
pl: CardBook<br>
pt-BR: CardBook<br>
pt-PT: CardBook<br>
ro: Contacte (CardBook)<br>
ru: CardBook<br>
sl: CardBook<br>
sv-SE: Kontakter (CardBook)<br>
uk: Контакти «CardBook»<br>
vi: <br>
zh-CN: CardBook<br>


### 2. Summary
cs: Nový adresář pro Thunderbird založený na standardech CardDAV a vCard.<br>
da: En ny Thunderbird-addressebog baseret på CardDAV- og vCard-standarderne.<br>
de: Ein neues Adressbuch für Thunderbird nach CardDAV und VCARD standard.<br>
el: Ένα νέο βιβλίο διευθύνσεων Thunderbird που βασίζεται στα πρότυπα CardDAV και εικονική κάρτα (vCard).<br>
en-US: **A new Thunderbird address book based on the CardDAV and vCard standards.**<br>
es: Un nuevo directorio de contactos para Thunderbird, basado en los estándares CardDAV y vCard.<br>
fr: Un nouveau carnet d'adresses pour Thunderbird basé sur les standards vCard et CardDAV.<br>
hr: <br>
hu: Névjegyzék (CardBook) alapul a CardDAV és a vCard szabványoknak.<br>
id: Buku alamat Thunderbird baru berdasarkan standar KartuDAV dan vKartu.<br>
it: Una nuova rubrica di Thunderbird basata sugli standard CardDAV e vCard.<br>
ja: CardDAVおよびvCard標準に基づく新しいThunderbirdアドレス帳<br>
ko: 카드 분산 저작 및 버전 관리(CardDAV) 및 가상 카드(vCard) 표준에 기반한 새로운 썬더버드(Thunderbird) 주소록입니다.<br>
lt: <br>
nl: Een nieuw adresboek voor gebruik in Thunderbird, gebaseerd op de standaards CardDav en vCard.<br>
pl: Nowa książka adresowa do Thunderbirda oparta na standardach CardDAV i vCard.<br>
pt-BR: Uma nova agenda.<br>
pt-PT: Uma nova agenda para o Thunderbird construída com os standards CardDAV e vCard.<br>
ro: O nouă adresă Thunderbird bazată pe standardele CardDAV și vCard.<br>
ru: Новая адресная книга для Thunderbird, основанная на стандартах CardDAV и
vCard.<br>
sl: Nov imenik za Thunderbird, ki je osnovan na standardih CardDAV in vCard.<br>
sv-SE: En ny Thunderbird adressbok baserad på CardDAV- och vCard-standarden.<br>
uk: Нова адресна книга Thunderbird на основі стандартів CardDAV та vCard.<br>
vi: Một cuốn sổ địa chỉ mới của Thunderbird dựa trên các tiêu chuẩn CardDAV và Thẻ Ảo (vCard).<br>
zh-CN: 一个新的基于CardDAV和vCard标准的Thunderbird通讯录。<br>

### 3. Image Captions

#### a) https://addons.thunderbird.net/user-media/previews/full/205/205504.png?modified=1554277935<br>
https://transvision.mozfr.org/?recherche=Classic+View&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: Klasický pohled<br>
da: Klassisk udseende<br>
de: Klassische Ansicht<br>
el: Κλασσική προβολή<br>
en-US: **Classic View**<br>
es-ES: Vista clásica<br>
fr: Vue classique<br>
hr: Klasični pogled<br>
hu: Klasszikus nézet<br>
id: Tampilan Klasik<br>
it: Visualizzazione classica<br>
ja: クラシック表示<br>
ko: 기본 보기<br>
lt: Numatytasis<br>
nl: Klassiek beeld<br>
pl: <br>
pt-BR: Clássico<br>
pt-PT: Clássico<br>
ro: Vedere clasică<br>
ru: Классический вид<br>
sl: Običajna<br>
sv-SE: Klassisk vy<br>
uk: Класичне<br>
vi: Hiển thị Cổ điển<br>
zh-CN: 经典视图<br>

#### b) https://addons.thunderbird.net/user-media/previews/full/205/205505.png?modified=1554277935<br>
https://transvision.mozfr.org/?recherche=Vertical+View&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: Svislý pohled<br>
da: Lodret udseende<br>
de: Vertikale Ansicht<br>
el: Κάθετη προβολή<br>
en-US: **Vertical View**<br>
es-ES: Vista vertical<br>
fr: Vue verticale<br>
hr: Okomiti pogled<br>
hu: Függőleges nézet<br>
id: Tampilan Tegak<br>
it: Visualizzazione verticale<br>
ja: 縦表示<br>
ko: 아래로 보기<br>
lt: Vertikalus<br>
nl: Verticaal beeld<br>
pl: Pionowy<br>
pt-BR: Vertical<br>
pt-PT: Vertical<br>
ro: Vedere verticală<br>
ru: Вертикальный вид<br>
sl: Navpična<br>
sv-SE: Vertikal vy<br>
uk: Вертикальне<br>
vi: Hiển thị Dọc<br>
zh-CN: 直视图<br>

#### c) https://addons.thunderbird.net/user-media/previews/full/205/205635.png?modified=1554277935
https://transvision.mozfr.org/?recherche=autocompletion&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
https://transvision.mozfr.org/?recherche=and&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings&entire_string=entire_string<br>
https://transvision.mozfr.org/?recherche=Contacts+Sidebar&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: Automatické doplňování adres a Lišta kontaktů<br>
da: Autofuldførelse for adresser og Kontakter<br>
de: Adress-Autovervollständigung und Kontakte-Sidebar<br>
el: Αυτόματη συμπλήρωση διεύθυνσης και Πλευρική στήλη επαφών<br>
en-US: **Address Autocompletion and Contacts Sidebar**<br>
es-ES: Autocompletado de direcciones y Panel lateral de contactos<br>
fr: Suggestion des emails et barre de contacts latérale<br>
hr: Automatsko dovršavanje adrese i Bočna traka kontakata<br>
hu: Önműködő címkiegészítés és Kapcsolatok oldalsáv<br>
id: Otomatis Melengkapi Alamat dan Bilah Samping Daftar Kenalan<br>
it: Completamento automatico indirizzi e Barra contatti<br>
ja: アドレスの自動補完 と アドレスサイドバー<br>
ko: 메일 주소 자동 완성 그리고 주소록 탐색창<br>
lt: Adreso užbaigimas pagal surinktą jo pradžią ir Parankinė (adresatai)<br>
nl: Automatische adresaanvulling en Adressenzijbalk<br>
pl: Automatyczne uzupełnianie adresu oraz Panel adresów<br>
pt-BR: Autocompletar endereços e Painel de contatos<br>
pt-PT: Conclusão automática de endereços e Painel de contactos<br>
ro: Completarea automată a adresei și Listă de contacte<br>
ru: Автодополнение адресов и Панель контактов<br>
sl: Samodokončanje naslova in Stranska vrstica stikov<br>
sv-SE: Automatisk komplettering av e-postadresser och Kontaktsidofältet<br>
uk: Автозавершення адреси та Панель контактів<br>
vi: Tự động điền Địa chỉ và Thanh Lề Danh Bạ<br>
zh-CN: 地址自动完成 并且 联系人侧栏<br>

#### d) https://addons.thunderbird.net/user-media/previews/full/205/205636.png?modified=1554277935
https://transvision.mozfr.org/?recherche=Address+Autocompletion&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Address Autocompletion in Lightning**<br>
es-ES: <br>
fr: Suggestion des contacts pour Lightning<br>
hr: <br>
hu: Önműködő címkiegészítés Lightningban<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### e) https://addons.thunderbird.net/user-media/previews/full/205/205508.png?modified=1554277935
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Search for Duplicate Contacts**<br>
es-ES: <br>
fr: Recherche des contacts dupliqués<br>
hr: <br>
hu: Ismételt névjegyék keresése<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### f) https://addons.thunderbird.net/user-media/previews/full/205/205509.png?modified=1554277935
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Merge Contacts**<br>
es-ES: <br>
fr: Fusion de contacts<br>
hr: <br>
hu: Névjegyek egyesítése<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### g) https://addons.thunderbird.net/user-media/previews/full/205/205637.png?modified=1554277935
https://transvision.mozfr.org/?recherche=Message+Header&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Manage Contacts from the Message Header**<br>
es-ES: <br>
fr: Gestion des contacts depuis l'écran principal<br>
hr: <br>
hu: Névjegyek kezelése az üzenetfejlécéből<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### h) https://addons.thunderbird.net/user-media/previews/full/205/205511.png?modified=1554277935
https://transvision.mozfr.org/?recherche=Filters&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings&entire_string=entire_string<br>
cs: Filtry<br>
da: Filtre<br>
de: Filter<br>
el: Φίλτρα<br>
en-US: **Filters**<br>
es-ES: Filtros<br>
fr: Filtres<br>
hr: Filteri<br>
hu: Szűrők<br>
id: Filter<br>
it: Filtri<br>
ja: フィルター<br>
ko: 필터<br>
lt: Filtrus<br>
nl: Filters<br>
pl: Filtry<br>
pt-BR: Filtros<br>
pt-PT: Filtros<br>
ro: Filtre<br>
ru: Фильтры<br>
sl: Filtri<br>
sv-SE: Filter<br>
uk: Фільтри<br>
vi: Bộ lọc<br>
zh-CN: 过滤器<br>

#### i) https://addons.thunderbird.net/user-media/previews/full/205/205512.png?modified=1554277935
https://transvision.mozfr.org/?recherche=Anniversary&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
https://transvision.mozfr.org/?recherche=Default+Reminder&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Anniversary Reminder**<br>
es-ES: <br>
fr: Liste des anniversaires<br>
hr: <br>
hu: Évforduló emlékeztető<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### j) https://addons.thunderbird.net/user-media/previews/full/205/205513.png?modified=1554277935
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Create Anniversary Events in Lightning**<br>
es-ES: <br>
fr: Export des anniversaires dans Lightning<br>
hr: <br>
hu: Évfordulói események létrehozása Lightningban<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### k) https://addons.thunderbird.net/user-media/previews/full/205/205514.png?modified=1554277935
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Import and Export Files (CSV, VCF)**<br>
es-ES: <br>
fr: Import et export des fichiers (CSV, VCF)<br>
hr: <br>
hu: Fájlok importálása és exportálása (CSV, VCF)<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### l) https://addons.thunderbird.net/user-media/previews/full/205/205515.png?modified=1554277935
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Standalone CardBook (thunderbird.exe -cardbook)**<br>
es-ES: <br>
fr: Utilisation de CardBook comme un programme indépendant (thunderbird.exe -cardbook)<br>
hr: <br>
hu: Önálló CardBook (thunderbird.exe -cardbook)<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

### 4. About this Add-on
#### cs:
Tento doplněk vám umožní spravovat všechny vaše kontakty pomocí standardu vCard.

Pokud si přejete zobrazit protokol změn, naleznete ho na této adrese:<br>
https://addons.thunderbird.net/cs/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Nahlásit problém: support@cardbook.icu
#### da:
Denne tilføjelse tillader dig at håndtere alle dine kontakter under vCard-standarden.

Versionshistorik: https://addons.thunderbird.net/da/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Rapportér et problem: support@cardbook.icu
#### de:
Dieses Plugin ermöglicht es ihre Kontakte nach dem VCARD Standard zu verwalten.

Changelog unter dieser URL: https://addons.thunderbird.net/de/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Problem melden: support@cardbook.icu
#### el:
Αυτό το πρόσθετο σάς επιτρέπει να διαχειρίζεστε όλες τις επαφές σας σύμφωνα με το πρότυπο εικονική κάρτα (vCard).

Ιστορικό εκδόσεων: https://addons.thunderbird.net/el/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Αναφορά προβλήματος: support@cardbook.icu
#### en-US:
<b>This add-on allows you to manage all your contacts under the vCard standard.</b>

<b>Version History: https://addons.thunderbird.net/en-US/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/</b>

<b>Report a problem: support@cardbook.icu</b>
#### es-ES:
Esta extensión te permite manejar todos tus contactos usando el estándar vCard.

Historial de versiones: https://addons.thunderbird.net/es-ES/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Informar de un problema: support@cardbook.icu
#### fr:
Ce plugin vous permet de gérer tous vos contacts avec le standard vCard.

Pour voir le journal des modifications, veuillez consulter cette URL : https://addons.thunderbird.net/fr/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab : CardBook<br>
Forum : https://cardbook.icu/forum/

Signaler un problème : support@cardbook.icu
#### hr: <br>
#### hu:
A Névjegyzék (CardBook) kiegészítő kezelheti az összes névjegyeket, a vCard szabvány alatt.

Verziótörténete : https://addons.thunderbird.net/hu/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Fórum: https://cardbook.icu/forum/

Probléma bejelentése: support@cardbook.icu
#### id:
Ekstensi ini memungkinkan Anda mengatur semua kontak Anda berdasarkan standar vKartu.

Informasi Versi: https://addons.thunderbird.net/id/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Laporkan masalah: support@cardbook.icu
#### it:
Questo componente permette di gestire tutti i tuoi contatti con lo
standard VCARD.

Clicca su questo link per visualizzare il registro delle modifiche:<br>
https://addons.thunderbird.net/it/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Segnala un problema: support@cardbook.icu
#### ja:
このアドオンでは、仮想カード「vCard」標準のすべての連絡先を管理できます。

バージョン情報: https://addons.thunderbird.net/ja/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

問題を報告: support@cardbook.icu
#### ko:
이 부가 기능을 사용하면 가상 카드(vCard) 표준에 따라 모든 연락처를 관리 할 수 있습니다.

버전 정보: https://addons.thunderbird.net/ko/thunderbird/addon/cardbook/versions<br>
Twitter: @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

오류 보고: support@cardbook.icu
#### lt: <br>
#### nl:
Deze plugin beheert uw contactpersonen, door gebruik te maken van de internationale vCard-standaard.

Voor een overzicht van de versies en wijzigingen, klik op deze URL: https://addons.thunderbird.net/nl/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Een probleem rapporteren: support@cardbook.icu
#### pl:
Ten dodatek pozwala zarządzać kontaktami w standardzie vCard.

Historia wersji: https://addons.thunderbird.net/pl/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Zgłoś problem: support@cardbook.icu
#### pt-BR:
Esse add-on permitirá organizar todos os seus contatos no padrão vCard.

Histórico de versões: https://addons.thunderbird.net/pt-BR/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Reportar um problema: support@cardbook.icu
#### pt-PT:
Esta extensão permite gerir todos os contactos através do standard VCARD.

Para verificar as diferenças de versões, consulte este URL: https://addons.thunderbird.net/pt-PT/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Fórum: https://cardbook.icu/forum/

Reportar um problema: support@cardbook.icu
#### ro:
Acest supliment vă permite să gestionați toate contactele vCard.

Informații privind versiunea: https://addons.thunderbird.net/ro/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Raportează o problemă: support@cardbook.icu
#### ru:
Это дополнение служит для управления контактами в соответствии со
стандартом vCard.

История версий: https://addons.thunderbird.net/ru/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Форум: https://cardbook.icu/forum/

Сообщить о проблеме: support@cardbook.icu
#### sl:
Dodatek omogoča upravljanje stikov v standardu vCard.

Zgodovina različic: https://addons.thunderbird.net/sl/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Prijavi težavo: support@cardbook.icu
#### sv-SE:
Med denna tillägg kan du hantera alla dina kontakter enligt vCard-standarden.

Versionsinformation: https://addons.thunderbird.net/sv-SE/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Rapportera ett problem: support@cardbook.icu
#### uk:
Цей надбудову дозволяє керувати всіма вашими контактами під стандартом vCard.

Історія версій: https://addons.thunderbird.net/uk/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Звіт про проблему: support@cardbook.icu
#### vi:
Tiện ích này cho phép bạn quản lý tất cả các địa chỉ liên lạc theo tiêu chuẩn Thẻ Ảo (vCard).

Thông tin về Phiên bản: https://addons.thunderbird.net/vi/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

Báo cáo một lỗi: support@cardbook.icu
#### zh-CN:此附件组件允许你管理你vCard标准下的全部联系人

版本历史: https://addons.thunderbird.net/zh-CN/thunderbird/addon/cardbook/versions<br>
Twitter : @CardBookAddon<br>
GitLab: CardBook<br>
Forum: https://cardbook.icu/forum/

反馈故障: support@cardbook.icu
