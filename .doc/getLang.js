		test: function () {
			if (!wdw_cardbook.testrun) {
				function launchReq(aCode) {
					console.debug("Run request for code : " + aCode);
					let xhr = new XMLHttpRequest();
					xhr.onload = function () {
						if (xhr.status === 200) {
							let country = xhr.responseURL.split(":")[2].split("&")[0].replace("region-name-", "");
							var container = document.implementation.createHTMLDocument().documentElement;
							container.innerHTML = xhr.responseText;
							let translations = container.querySelectorAll("td[lang]");
							for (let translation of translations) {
								let lang = translation.lang.replace("#", "");
								if (langs.includes(lang)) {
									if (!result[lang]) {
										result[lang] = {};
									}
									result[lang][country] = translation.textContent;
								}
							}
						} else {
							console.debug("error3");
						}
						requestFinished++
					};
					xhr.onerror = function () {
						requestFinished++
						console.debug("error1");
					};
					xhr.ontimeout = function () {
						requestFinished++
						console.debug("error2");
					};
					xhr.open("GET", `https://transvision.mozfr.org/string/?entity=toolkit/toolkit/intl/regionNames.ftl:region-name-${aCode}&repo=gecko_strings`, true);
					xhr.send();
				}
				wdw_cardbook.testrun = true;
				let result = {};
				let limit = 10;
				let countriesList = ["ad","ae","af","ag","ai","al","am","ao","aq","ar","as","at","au","aw","az","ba","bb","bd","be","bf","bg",
				"bh","bi","bj","bl","bm","bn","bo","bq","br","bs","bt","bv","bw","by","bz","ca","cc","cd","cf","cg","ch",
				"ci","ck","cl","cm","cn","co","cp","cr","cu","cv","cw","cx","cy","cz","de","dg","dj","dk","dm","do","dz",
				"ec","ee","eg","eh","er","es","et","fi","fj","fk","fm","fo","fr","ga","gb","gd","ge","gf","gg","gh","gi",
				"gl","gm","gn","gp","gq","gr","gs","gt","gu","gw","gy","hk","hm","hn","hr","ht","hu","id","ie","il","im",
				"in","io","iq","ir","is","it","je","jm","jo","jp","ke","kg","kh","ki","km","kn","kp","kr","kw","ky","kz",
				"la","lb","lc","li","lk","lr","ls","lt","lu","lv","ly","ma","mc","md","me","mf","mg","mh","mk","ml","mm",
				"mn","mo","mp","mq","mr","ms","mt","mu","mv","mw","mx","my","mz","na","nc","ne","nf","ng","ni","nl","no",
				"np","nr","nu","nz","om","pa","pe","pf","pg","ph","pk","pl","pm","pn","pr","pt","pw","py","qa","qm","qs",
				"qu","qw","qx","qz","re","ro","rs","ru","rw","sa","sb","sc","sd","se","sg","sh","si","sk","sl","sm","sn",
				"so","sr","ss","st","sv","sx","sy","sz","tc","td","tf","tg","th","tj","tk","tl","tm","tn","to","tr","tt",
				"tv","tw","tz","ua","ug","us","uy","uz","va","vc","ve","vg","vi","vn","vu","wf","ws","xa","xb","xc","xd",
				"xe","xg","xh","xj","xk","xl","xm","xp","xq","xr","xs","xt","xu","xv","xw","ye","yt","za","zm","zw"];
				let langs = [ "ar", "cs", "da", "de", "el", "en-US", "es-ES", "fr", "hr", "hu", "id", "it", "ja", "ko", "lt", "nl", "pl", "pt-BR",
								"pt-PT", "ro", "ru", "sk", "sl", "sv-SE", "tr", "uk", "vi", "zh-CN"];
				let requestLaunched = 0;
				let requestFinished = 0;
				console.debug("Initial runs");
				for (let code of countriesList) {
					if (requestLaunched >= limit) {
						continue;
					}
					launchReq(code)
					requestLaunched++;

				}
				var timer = Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer);
				timer.initWithCallback({ notify: function(timer) {
					console.debug("In the timer");
					console.debug("In the timer : " + requestLaunched + " : requestFinished : " + requestFinished);
						if (requestLaunched == requestFinished) {
							requestLaunched = 0;
							requestFinished = 0;
							let finished = true;
							for (let code of countriesList) {
								if (!result["en-US"][code]) {
									if (requestLaunched >= limit) {
										continue;
									}
									launchReq(code);
									requestLaunched++;
									finished = false;
								}
							}
							console.debug(finished)
							if (finished === true) {
								for (let lang of langs) {
									let str = "";
									let file = cardbookRepository.getLocalDirectory();
									file.append(lang + ".json");
									for (let code in result[lang]) {
										str = str + "	\"region-name-" + code + "\": {\r\n";
										str = str + "		\"message\": \"" + result[lang][code] + "\"\r\n";
										str = str + "	},\r\n";
									}
									cardbookRepository.cardbookUtils.writeContentToFile(file.path, str, "UTF8");
								}
								console.debug(result)
								timer.cancel();
							}
						}
					}
				}, 1000, Components.interfaces.nsITimer.TYPE_REPEATING_SLACK);
			}
		},
