<clientConfig version="1.1">
	<carddavProvider id="CardDAVEngine">
		<carddavURL>https://carddav.mykolab.com/.well-known/carddav</carddavURL>
		<vCardVersion>3.0</vCardVersion>
	</carddavProvider>
</clientConfig>